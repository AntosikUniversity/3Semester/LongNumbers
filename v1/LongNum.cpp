#ifndef __LONG_NUM__
#include "LongNum.h"
#endif // __LONG_NUM__

/* init */
LongNum::LongNum() {
	*this = 0;
}

LongNum::LongNum(const MyString & string) {
	if (string.chars()[0] == '-') {
		digits = string.substr(1, string.length() - 1);
		sign = false;
	} else {
		digits = string;
		sign = true;
	}
}

LongNum::LongNum(const int number) {
	digits = number;
	sign = (number < 0) ? false : true;
}

LongNum::LongNum(const LongNum & number) {

	sign = number.sign;
	digits = number.digits;
}


/* desctructor */
LongNum::~LongNum() {
}


/* get & set */
bool LongNum::GetSign() const {
	return sign;
}

const MyString & LongNum::GetDigits() const {
	return digits;
}

void LongNum::SetSign(bool value) {
	sign = value;
}

void LongNum::SetDigits(const MyString & digitsStr) {
	digits = digitsStr;
}


/* operators */

// arithmetical
LongNum LongNum::operator-() const {
	LongNum result;
	result = *this;
	result.SetSign(!sign);

	return result;
}

LongNum & LongNum::operator=(const LongNum & right) {
	this->digits = right.digits;
	this->sign = right.sign;

	return *this;
}

LongNum & LongNum::operator+=(const LongNum & right) {
	*this = *this + right;

	return *this;
}

LongNum & LongNum::operator-=(const LongNum & right) {
	*this = *this - right;

	return *this;
}

LongNum & LongNum::operator*=(const LongNum & right) {
	*this = *this * right;

	return *this;
}

LongNum & LongNum::operator/=(const LongNum & right) {
	*this = *this / right;

	return *this;
}

LongNum operator+(const LongNum & left, const LongNum & right) {
	LongNum result;

	if (left.sign == right.sign) {
		result.sign = left.sign;
		if (left.digits.length() > right.digits.length())
			result.digits = sum(left.digits, right.digits);
		else
			result.digits = sum(right.digits, left.digits);
	} else {
		if (left == -right)
			return 0;

		if (left.sign) {
			if (left > -right) {
				result.sign = true;
				result.digits = diff(left.digits, right.digits);
			} else {
				result.sign = false;
				result.digits = diff(right.digits, left.digits);
			}
		} else {
			if (left < -right) {
				result.sign = false;
				result.digits = diff(left.digits, right.digits);
			} else {
				result.sign = true;
				result.digits = diff(right.digits, left.digits);
			}
		}
	}

	return result;
}

LongNum operator-(const LongNum & left, const LongNum & right) {
	return left + -right;
}

LongNum operator*(const LongNum & left, const LongNum & right) {
	LongNum result;

	if (left == 0 || right == 0)
		return 0;
	if (left == 1)
		return right;
	if (left == -1)
		return -right;
	if (right == 1)
		return left;
	if (right == -1)
		return -left;

	if (left.sign == right.sign) {
		result.sign = true;
	} else {
		result.sign = false;
	}

	if (left.digits.length() > right.digits.length()) {
		result.digits = multi(left.digits, right.digits);
	} else {
		result.digits = multi(right.digits, left.digits);
	}
	return result;
}

LongNum abs(const LongNum & left) {
	LongNum res;
	res.SetSign(true);
	res.SetDigits(left.GetDigits());
	return res;
}

LongNum operator/(const LongNum & left, const LongNum & right) {
	if (right == 0) {
		std::cout << "ERROR! Division Zero" << std::endl;
		return 0;
	}
	if (left == right)
		return 1;
	if (left == -right)
		return -1;
	if (right == 1)
		return left;
	if (right == -1)
		return -left;
	if (left == 0)
		return 0;
	if (left == 1)
		return right;
	if (left == -1)
		return -right;
	if (abs(left) < abs(right)) {
		return 0;
	}
	LongNum result;
	result.sign = left.sign && right.sign;
	result.digits = division(left.digits, right.digits);
	return result;
}


// bool
bool operator==(const LongNum & left, const LongNum & right) {
	if (left.sign != right.sign)
		return false;

	if (left.digits.length() != right.digits.length())
		return false;

	unsigned short int size = left.digits.length();
	for (unsigned short int i = 0; i < size; i++)
		if (left.digits.chars()[i] != right.digits.chars()[i])
			return false;

	return true;
}

bool operator!=(const LongNum & left, const LongNum & right) {
	return !(left == right);
}

bool operator>(const LongNum & left, const LongNum & right) {
	if (left.sign && !right.sign) return true;
	if (!left.sign && right.sign) return false;

	if (left.digits.length() > right.digits.length()) {
		if (left.sign) return true;
		else return false;
	}

	if (left.digits.length() < right.digits.length()) {
		if (left.sign) return false;
		else return true;
	}

	unsigned short int size = left.digits.length();
	for (unsigned short int i = 0; i < size; ++i) {
		if (left.digits.chars()[i] != right.digits.chars()[i]) {
			if (left.digits.chars()[i] > right.digits.chars()[i]) {
				if (left.sign) return true;
				else return false;
			} else {
				if (left.sign) return false;
				else return true;
			}
		}
	}
	return false;
}

bool operator<(const LongNum & left, const LongNum & right) {
	return !((left > right) || (left == right));
}

bool operator>=(const LongNum & left, const LongNum & right) {
	return ((left > right) || (left == right));
}

bool operator<=(const LongNum & left, const LongNum & right) {
	return !(left > right);
}


// input & output
std::ostream & operator<<(std::ostream & os, const LongNum & a) {
	if (!a.sign) os << '-';
	if (a.digits.length() == 0) os << 0;
	else os << a.digits;
	return os;
}

std::istream & operator >> (std::istream & is, LongNum & a) {
	MyString str;
	is >> str;
	a = str;
	return is;
}
