#include <iostream>
#include <fstream>
#include "MyString.h"
#include "LongNum.h"
#include "MyStack.h"

using namespace std;

const char* FileInputName = "input.txt";
int Priority(const char oper);

int main() {
	ifstream FileInput;
	FileInput.open(FileInputName, ios::in);

	char c = ' ', _c = ' ';
	char nowChar = ' ', topChar = ' ';
	MyString Number;

	LongNum Prev(0), Now(0), Result(0);
	MyStack<char> operators('(');
	MyStack<LongNum> LongNums(LongNum(0));

	while (!FileInput.eof()) {
		FileInput >> c;
		if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%' || c == '(' || c == ')' || c == ';') {
			if (isdigit(_c)) {
				LongNums.pushP(new LongNum(Number));
				Number.clear();
			}
			if (c == '(') {
				operators.push(c);
			} else if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%') {
				if (Priority(c) <= Priority(*operators.getEl())) {
					Now = LongNums.pop(); Prev = LongNums.pop();
					switch (nowChar = operators.pop()) {
						case '+': Result = Prev + Now; cout << '\n' << Prev << " + " << Now << " = " << Result; break;
						case '-': Result = Prev - Now; cout << '\n' << Prev << " - " << Now << " = " << Result; break;
						case '*': Result = Prev * Now; cout << '\n' << Prev << " * " << Now << " = " << Result; break;
						case '/': Result = Prev / Now; cout << '\n' << Prev << " / " << Now << " = " << Result; break;
						default: printf("Undefined!"); break;
					}
					LongNums.pushP(&Result);
				}
				operators.push(c);
			} else if (c == ')' || c == ';') {
				do {
					nowChar = operators.pop();
					int asd = operators.length();
					if (nowChar == '(' || nowChar == ' ') break;
					Now = LongNums.pop(); Prev = LongNums.pop();
					switch (nowChar) {
						case '+': Result = Prev + Now; cout << '\n' << Prev << " + " << Now << " = " << Result; break;
						case '-': Result = Prev - Now; cout << '\n' << Prev << " - " << Now << " = " << Result; break;
						case '*': Result = Prev * Now; cout << '\n' << Prev << " * " << Now << " = " << Result; break;
						case '/': Result = Prev / Now; cout << '\n' << Prev << " / " << Now << " = " << Result; break;
						default: printf("Undefined!"); break;
					}
					LongNums.pushP(&Result);
				} while (nowChar != '(' && nowChar != ' ');
				if (nowChar == '(' && c == ';') {
					char cc = '(';
					operators.clear(); LongNums.clear();
					operators.push(cc);
					LongNums.pushP(new LongNum(0));
					Prev = 0; Now = 0; Result = 0;
					cout << "\n\n-----------------------------------\n";
				}
			}
		} else if (isdigit(c)) {
			Number.insert(Number.length(), c);
		};
		_c = c;
	}

	FileInput.close();
	system("pause");
	return 0;
}

int Priority(const char oper) { // return Priority 1 - +-; 2 - */; 3 - (
	switch (oper) {
		case '(': case ')': return 0;
		case '+': case'-': return 1;
		case '*': case'/': case'%': return 2;
		default:
			break;
	}
}