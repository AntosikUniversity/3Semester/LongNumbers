#ifndef __LONG_NUM__
#define  __LONG_NUM__

#ifndef __MY_STRING__
#include "MyString.h"
#endif // __MY_STRING__

#ifndef __LONG_NUM_HELPERS__
#include "LongNumHelpers.h"
#endif // __LONG_NUM_HELPERS__

#include <iostream>

class LongNum {
private:
	bool sign; // true +, false -
	MyString digits;

public:
	/* init */
	LongNum();
	LongNum(const MyString &);
	LongNum(const int);
	LongNum(const LongNum &);

	/* desctructor */
	~LongNum();

	/* get & set */
	bool GetSign() const;
	const MyString & GetDigits() const;
	void SetSign(bool value);
	void SetDigits(const MyString&);

	/* operators */

	// arithmetical
	LongNum operator- () const;
	LongNum & operator = (const LongNum &);

	friend LongNum operator + (const LongNum &, const LongNum &);
	friend LongNum operator - (const LongNum &, const LongNum &);
	friend LongNum operator * (const LongNum &, const LongNum &);
	friend LongNum operator / (const LongNum &, const LongNum &);

	LongNum & operator += (const LongNum &);
	LongNum & operator -= (const LongNum &);
	LongNum & operator *= (const LongNum &);
	LongNum & operator /= (const LongNum &);

	// bool
	friend bool operator == (const LongNum &, const LongNum &);
	friend bool operator != (const LongNum &, const LongNum &);
	friend bool operator > (const LongNum &, const LongNum &);
	friend bool operator < (const LongNum &, const LongNum &);
	friend bool operator >= (const LongNum &, const LongNum &);
	friend bool operator <= (const LongNum &, const LongNum &);

	// input & output
	friend std::ostream & operator <<(std::ostream & os, const LongNum & a);
	friend std::istream & operator >> (std::istream & is, LongNum & a);
};

#endif // __LONG_NUM__