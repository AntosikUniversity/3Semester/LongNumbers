﻿#ifndef __LONG_NUM_HELPERS__
#include "LongNumHelpers.h"
#endif // __LONG_NUM_HELPERS__

/*
	a - первый элемент, b - второй
	функция возвращает MyString
	a ВСЕГДА больше b

	Переменные:
	unsigned short int asize, bzise - длины наших чисел
	unsigned short int as, bs - длины-1
	short int value - сумма цифр двух чисел
	bool add - нужно ли добавлять в следующий разряд +1

	Описание работы:
	В первом цикле мы считаем по меньшему разряду (числу b) и складываем цифры.
	Если число больше 9 - прибавляем +1 к следющему разряду
	Во втором цикле мы обрабатываем цифры большего числа, т.к. может получиться, что нужно больше разрядов (999 + 1)
	Если размера полученного числа не хватает - увеличиваем его с помощью setlength
*/

MyString sum(const MyString & a, const MyString & b) {
	MyString result = a;
	unsigned short int bsize = b.length(), asize = a.length();
	unsigned short int bs = bsize - 1, i, as = asize - 1;
	short int value;
	bool add = false;
	for (i = 0; i < bsize; i++) {
		value = a.chars()[as - i] + b.chars()[bs - i] - 96;
		if (add) {
			value++;
			add = false;
		}
		if (value > 9) {
			value %= 10;
			add = true;
		}
		result.chars()[as - i] = value + 48;
	}

	while (add && i < asize) {
		value = a.chars()[as - i] - 47;
		add = false;
		if (value > 9) {
			value %= 10;
			add = true;
		}
		result.chars()[as - i] = value + 48;
		i++;
	}

	if (add) {
		int curLen = result.length();
		result.setlength(curLen + 1);
		result.chars()[curLen + 1] = '1';
	}

	return result;
};




/*
	a - первый элемент, b - второй
	функция возвращает MyString
	a ВСЕГДА больше b

	Переменные:
	unsigned short int asize, bzise - длины наших чисел
	unsigned short int as, bs - длины-1
	short int value - сумма цифр двух чисел
	bool decreases - нужно ли добавлять в следующий разряд -1

	Описание работы:
	В первом цикле мы считаем по меньшему разряду (числу b) и вычитаем поразраядно из первого числа цифры второго.
	Если число меньше 0 - вычитаем 1 из следющего разряда
	Во втором цикле мы обрабатываем цифры большего числа, т.к. может получиться, что нужно отнять из больших разрядов (1000 - 1)
	В третьем мы определяем, есть ли у нас нули в разрядах. Если да - удаляем.
*/

MyString diff(const MyString & a, const MyString & b) {
	MyString result = a;
	unsigned short int bsize = b.length(), asize = a.length();
	unsigned short int bs = bsize - 1, i, as = asize - 1;
	short int value;
	bool decreases = false;
	for (i = 0; i < bsize; i++) {
		if (decreases) {
			decreases = false;
			value = a.chars()[as - i] - b.chars()[bs - i] - 1;
		} else {
			value = a.chars()[as - i] - b.chars()[bs - i];
		}

		if (value < 0) {
			decreases = true;
			value += 10;
		}

		result.chars()[as - i] = value + 48;
	}

	while (decreases && i < asize) {
		value = a.chars()[as - i] - 49;
		decreases = false;
		if (value < 0) {
			value += 10;
			decreases = true;
		}
		result.chars()[as - i] = value + 48;
		i++;
	}

	for (i = 0; i < asize; i++)
		if (result.chars()[i] != '0')
			break;

	if (i == asize)
		result = result.substr(i - 1, result.length() - i + 1);
	else
		result = result.substr(i, result.length() - i);

	return result;
};



/*
	a - первый элемент, b - второй (цифра)
	a ВСЕГДА больше b
	функция возвращает MyString

	Переменные:
	MyString result - наше получившееся число
	short int add = 0 - число, которое нужно добавить в следующий разряд
	short int value - цифра в разряде

	Механизм:
	В 1 цикле мы поразрядно умножаем на a на цифру b и суммируем результаты.
		Каждое число после умножения делим нацело на 10: остаток записываем в разряд результата, а целое записываем в add
	После мы проделываем то же самое с первым разрядом.
	Если нам все еще нужно добавить что-то в разряд выше, то добавляем его с помощью функции insert
*/
MyString multi(const MyString & a, short int b) {
	MyString result = a;
	short int add = 0, value;

	for (unsigned short int i = a.length() - 1; i > 0; i--) {
		value = (a.chars()[i] - 48) * b + add;
		add = 0;
		if (value > 9) {
			add = value / 10;
			value %= 10;
		}
		result.chars()[i] = value + 48;
	}

	value = (a.chars()[0] - 48) * b + add;
	add = 0;
	if (value > 9) {
		add = value / 10;
		value %= 10;
	}
	result.chars()[0] = value + 48;

	if (add != 0) {
		char ins[2] = { add + 48, '\0' }, *pIns = ins;
		result.insert(0, pIns);
	}

	return result;
}


/*
	a - первый элемент, b - второй
	a ВСЕГДА больше b
	функция возвращает MyString

	Переменные:
	LongNum result - наше получившееся число
	MyString _a - число, которым мы манипулируем
	short int value - цифра в разряде

	Механизм:
	В 1 цикле мы поразрядно умножаем на _a на цифры b с помощью функции 'multi', попутно добавляя 0 в конец числа _a
	Во втором условии мы проверяем, является ли последний разряд единицей или нет, после чего умножаем

	P.S. Проверка на value == 1 нужна, чтобы оптимизировать работу функции (минус лишняя работа)
*/
MyString multi(const MyString & a, const MyString & b) {
	LongNum result;
	MyString _a = a;

	for (unsigned short int i = b.length() - 1; i > 0; i--) {
		short int value = b.chars()[i] - 48;

		if (value == 1)
			result += _a;
		else
			if (value > 1)
				result += multi(_a, value);
		char ins[2] = { '0', '\0' }, *pIns = ins;
		_a.insert(_a.length(), pIns);
	}

	short int value = b.chars()[0] - 48;

	if (value == 1)
		result += _a;
	else
		if (value > 1)
			result += multi(_a, value);

	return result.GetDigits();
}


/*
	a - первый элемент, b - второй
	функция возвращает MyString

	Переменные:
	LongNum result - наше получившееся число
	MyString a1, b1 - копии чисел a и b, которым мы манипулируем
	short int value - разность размеров чисел

	Механизм:
	1 проверка: Если у нас первая цифра первого числа больше чем первая цифра второго, то мы просто домножаем второе число на разность разрядов
	2 проверка: Если у нас первая цифра первого числа равна первой цифре второго, то мы просто домножаем второе число, пока оно не будет по длине как первое
				Если же при этом первое число меньше второго, то т.к. это деление нацело (div), мы уменьшаем количество разрядов второго числа на 1
				После этого так же домножаем второе число, пока оно не будет по длине как первое
	3 проверка: Если у нас первая цифра первого числа меньше первой цифры второго, то мы уменьшаем количество value
				После этого так же домножаем второе число, пока оно не будет по длине как первое
	Цикл while: Пока у нас первое число и количество разрядов больше 0, мы 
					1) минусуем из первого числа второе и получаем результат
					2) Увеличиваем количество разрядов конечного числа
					3) Пишем в последний разряд результат (1)
					4) "Обрубаем" последнюю цифру второго числа
	Если первое число уже равно нулю, но у нас разность разрядов еще не равна нулю, то домножаем результат, пока value не будет равен 0

	Пример действия:
		Делим 121 на 11
		Проверки: преобразуем 11 до 110
		Цикл:
			1) a1 = 121; b1 = 110; value = 1; x = 1; → result = 1;
			1) a1 = 121 - 110 = 11; b1 = 11; value = 0; x = 1; → result = 1*10 + 1 = 11;

*/
MyString division(const MyString & a, const MyString & b) {
	LongNum a1(a), b1(b), result;
	unsigned short int value = 0;

	value = a1.GetDigits().length() - b1.GetDigits().length();

	if (a1.GetDigits().chars()[0] > b1.GetDigits().chars()[0]) {
		for (int i = 0; i < value; i++)
			b1 *= 10;
	}

	if (a1.GetDigits().chars()[0] == b1.GetDigits().chars()[0]) {
		for (int i = 0; i < value; i++)
			b1 *= 10;

		if (a1 < b1) {
			b1.SetDigits(b1.GetDigits().substr(0, b1.GetDigits().length() - 1));
			value--;
		}
	}

	if (a1.GetDigits().chars()[0] < b1.GetDigits().chars()[0]) {
		value--;
		for (int i = 0; i < value; i++)
			b1 *= 10;
	}

	while (a1 > 0 && value > 0) {
		short int x = 0;
		while (a1 >= b1) {
			a1 -= b1;
			x++;
		}

		result *= 10;
		b1.SetDigits(b1.GetDigits().substr(0, b1.GetDigits().length() - 1));
		result += x;
		value--;
	}
	if (a1 > 0) {
		short int x = 0;
		while (a1 >= b1) {
			a1 -= b1;
			x++;
		}
		result *= 10;
		result += x;
	}
	if (value > 0)
		for (int i = 0; i < value; i++)
			result *= 10;

	return result.GetDigits();
}