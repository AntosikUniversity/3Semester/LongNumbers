#ifndef __MY_STRING__
#include "MyString.h"
#endif // __MY_STRING__

#ifndef __LONG_NUM_HELPERS__
#define  __LONG_NUM_HELPERS__

#ifndef __LONG_NUM__
#include "LongNum.h"
#endif // __LONG_NUM__

#ifndef __MY_STRING__
#include "MyString.h"
#endif // __MY_STRING__

MyString sum(const MyString & a, const MyString & b);

MyString diff(const MyString & a, const MyString & b);

MyString multi(const MyString & a, short int b);
MyString multi(const MyString & a, const MyString & b);

MyString division(const MyString & a, const MyString & b);

#endif // __LONG_NUM_HELPERS__