#ifndef  __MY_STRING__
#define  __MY_STRING__

#include <string.h>
#include <iostream>

int gorn(char * ptr, int number, int osn = 10);

class MyString {
	private:
	char * _data;
	int _length;
	public:
	/* init */
	MyString(int num);
	MyString(char * x = 0);
	MyString(const MyString &);

	/* desctructor */
	void clear();
	~MyString();

	/* get & set */
	char * chars() const;									// ��������� ������� ��������
	int length() const;										// ����� �����
	void setlength(int len);								// �������� ����� �����
	MyString insert(int pos, char newchar);					// ��������� ������ �� �������
	MyString insert(int pos, char * chars);					// ��������� ������ �������� �� �������
	MyString substr(int start, int count) const;			// �������� count �������� ������� � ������� start
	int find(char ch) const;								// �������� ������� ������� � �����

	/* operators */

	// arithmetical
	MyString operator += (const MyString & second);
	MyString operator + (const MyString & second);

	// bool
	MyString & operator = (const MyString & second);
	int operator == (const MyString & second);
	int operator != (const MyString & second);

	// input & output
	std::istream & read(std::istream & stream);						// ������ �� �������
	std::ostream & write(std::ostream & stream) const;
	MyString readLine(std::istream & stream);						// ������ �� ����� ������
	friend std::ostream & operator << (std::ostream & stream, const MyString & el);
	friend std::istream & operator >> (std::istream & stream, MyString & el);
};

#endif //  __MY_STRING__